package com.qf.mrboot.controller;

import com.qf.mrboot.entity.Pic;
import com.qf.mrboot.result.ResponseData;
import com.qf.mrboot.service.PicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * (Pic)表控制层
 *
 * @author makejava
 * @since 2021-05-26 10:24:35
 */
@Api(value = "PicAPI")
@RestController
@RequestMapping("pic")
public class PicController {
    /**
     * 服务对象
     */
    @Autowired
    private PicService picService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    /*Restful风格*/
    @ApiOperation(value = "selectOne",notes = "通过图片id获取到对应图片信息")
    @ApiImplicitParam(name = "id",value = "图片的id",dataType = "int")
    @GetMapping("id/{id}")
    public Pic selectOne(@PathVariable Integer id) {
        return this.picService.queryById(id);
    }


    //实现数据的获取  短信认证
    @ApiOperation(value = "queryByType",notes = "通过图片类型获取图片对应列表")
    @ApiImplicitParam(name = "type",value = "图片类型")
    @GetMapping("queryByType/{type}")
    public ResponseData queryPicByType(String type){
        return picService.queryByType(type);
    }




    /*@ApiOperation(value = "selectType",notes = "通过图片类型获取到对应图片列表信息")
    @ApiImplicitParam(name = "type",value = "图片的类型")*/
    @ApiIgnore
    @GetMapping("type/{type}")
    public Pic selectByType(String type) {
        Integer id=1;
        return this.picService.queryById(id);
    }

    //参数三个以上考虑用对象进行封装
    /*@ApiImplicitParams({
        @ApiImplicitParam(name = "uname",value = "用户名"),
        @ApiImplicitParam(name = "tel",value = "电话号码")
    })*/
    /*@RequestMapping(value = "insert",method = RequestMethod.GET)*/
    @ApiOperation(value = "insert",notes = "添加预约信息")
    @ApiImplicitParam(name = "pic",value = "图片对象")
    @PostMapping("insert")
    public Pic insert(@RequestBody Pic pic) {
        return pic;
    }

}
