package com.qf.mrboot.controller;

import com.qf.mrboot.entity.Order;
import com.qf.mrboot.result.ResponseData;
import com.qf.mrboot.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Order)表控制层
 *
 * @author makejava
 * @since 2021-06-02 09:46:03
 */
@RestController
@RequestMapping("order")
public class OrderController<Orders> {
    /**
     * 服务的对象
     */
    @Resource
    private OrderService orderService;

    /**
     * 通过主键查询单条数据
     * chaxuans s 
     *
     */
    @GetMapping("selectOne")
    public Orders  selectOne(Integer id) {
        return (Orders) this.orderService.queryById(id);
    }
    @ApiOperation(value = "createOrder",notes = "添加预约的记录")
    @PostMapping("createOrder")
    public ResponseData createOrder(@RequestBody Order order){
        //也可以在这里手动设置事务回滚232434
        return  orderService.insert(order);
    }

}
