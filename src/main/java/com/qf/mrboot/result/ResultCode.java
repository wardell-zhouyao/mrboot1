package com.qf.mrboot.result;

public enum ResultCode {
    SUCCESS("200", "操作成功"),
    FAILED("999999","操作失败"),
    ParamError("000001", "参数错误！"),
    FileEmpty("000400","上传文件是空"),
    FileType("000403","文件类型错误"),
    LimitPictureSize("000401","图片大小必须小于2M"),
    LimitPictureType("000402","图片的格式必须为'jpg'、'png'、'jpge'、'gif'、'bmp'");

    private String eCode;
    private String errorInfo;

    ResultCode(String eCode, String errorInfo) {
        this.eCode = eCode;
        this.errorInfo = errorInfo;
    }

    public String geteCode() {
        return eCode;
    }

    public void seteCode(String eCode) {
        this.eCode = eCode;
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }
}
