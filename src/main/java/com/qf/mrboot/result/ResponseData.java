package com.qf.mrboot.result;


public class ResponseData {
    private String eCode;
    private String errorInfo;
    private Object data;

    //自定义错误
    public ResponseData(String eCode, String errorInfo, Object data) {
        this.eCode = eCode;
        this.errorInfo = errorInfo;
        this.data = data;
    }


    public ResponseData(ResultCode resultCode, Object data) {
        this.eCode = resultCode.geteCode();
        this.errorInfo = resultCode.getErrorInfo();
        this.data = data;
    }


    public String geteCode() {
        return eCode;
    }

    public void seteCode(String eCode) {
        this.eCode = eCode;
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
