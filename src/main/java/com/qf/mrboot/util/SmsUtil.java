package com.qf.mrboot.util;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.HashMap;
import java.util.UUID;

public class SmsUtil {

    /*
    pom.xml
    <dependency>
      <groupId>com.aliyun</groupId>
      <artifactId>aliyun-java-sdk-core</artifactId>
      <version>4.5.16</version>
    </dependency>
    */

    public static String SendMs(String phoneNumber){
        DefaultProfile profile = DefaultProfile.getProfile("cn-shenzhen", "LTAI5tBD7Yr5SqPXGbtBvETx", "xoQecEfUEYVcgSX4JevIlEhNJISLjW");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");

        request.putQueryParameter("PhoneNumbers", phoneNumber);
        request.putQueryParameter("SignName", "阳阳商城");
        request.putQueryParameter("TemplateCode", "SMS_205409285");

        String code = UUID.randomUUID().toString().substring(0, 4);
        HashMap<String, String> map = new HashMap<>(1);
        map.put("code",code);


        //必须是json格式
        request.putQueryParameter("TemplateParam", JSON.toJSONString(map));
        try {
            CommonResponse response = client.getCommonResponse(request);
            boolean success = response.getHttpResponse().isSuccess();
            if (success){
                return code;
            }else {
                return null;
            }
        } catch (ClientException e) {
            e.printStackTrace();
            return null;
        }
    }

}
