package com.qf.mrboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.qf.mrboot.dao")
@EnableTransactionManagement
public class MrbootApplication {
    //    新建测试类
    public static void main(String[] args) {
        SpringApplication.run(MrbootApplication.class, args);
    }

}
